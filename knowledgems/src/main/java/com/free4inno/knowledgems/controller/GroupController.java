package com.free4inno.knowledgems.controller;

import com.free4inno.knowledgems.dao.GroupInfoDao;
import com.free4inno.knowledgems.dao.UserDao;
import com.free4inno.knowledgems.dao.UserGroupDao;
import com.free4inno.knowledgems.domain.GroupInfo;
import com.free4inno.knowledgems.domain.User;
import com.free4inno.knowledgems.domain.UserGroup;
import com.free4inno.knowledgems.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

import java.util.List;
import java.util.Map;

/**
 * GroupController.
 */
@Slf4j
@Controller
@RequestMapping("/group")
public class GroupController {

    @Autowired
    private UserGroupDao userGroupDao;

    @Autowired
    private GroupInfoDao groupInfoDao;

    @Autowired
    private UserDao userDao;

    @RequestMapping("/group")
    public String getGroup(Map param, HttpSession session) {
        log.info(this.getClass().getName() + "----in----" + "群组(group)" + "----" + session.getAttribute(Constants.USER_ID));

        int userId = (Integer) session.getAttribute(Constants.USER_ID);
        List<UserGroup> userGroups = userGroupDao.findByUserId(userId);

        for (UserGroup userGroup : userGroups) {
            int groupId = userGroup.getGroupId();
            int permission1 = 0;
            int memberNum = 0;
            List<UserGroup> leaderGroups = userGroupDao.findByGroupId(groupId);
            if (leaderGroups != null) {
                memberNum = leaderGroups.size();
                int flag = 0; //是否有组长
                userGroup.setMemberNum(memberNum);
                for (UserGroup leaderGroup : leaderGroups) {
                    permission1 = leaderGroup.getPermission();
                    if (permission1 == 1) {
                        User user = userDao.findById(leaderGroup.getUserId()).orElse(new User());
                        userGroup.setLeader(user.getRealName());
                        flag = 1;
                    }
                    if (flag == 0){
                        userGroup.setLeader("未知群主");
                    }
                }
            }

            //取组名、创建时间、描述
            GroupInfo groupInfo1 = groupInfoDao.findById(groupId).orElse(new GroupInfo());
            if (groupInfo1.getGroupName() == "") {
                userGroup.setGroupName("未知组名");
            } else {
                userGroup.setGroupName(groupInfo1.getGroupName());
            }

            if (groupInfo1.getGroupInfo() == "") {
                userGroup.setGroupInfo("群主是个很忙（lan）的人，所以没有写群组介绍~");

            } else {
                userGroup.setGroupInfo(groupInfo1.getGroupInfo());
            }

            if (groupInfo1.getCreateTime() != null) {
                userGroup.setCreateTime(groupInfo1.getCreateTime());
            } else {
            }
        }

        param.put("userGroups", userGroups);
        log.info(this.getClass().getName() + "----out----" + "跳转到群组页面" + "----" + session.getAttribute(Constants.USER_ID));
        return "group/group";
    }
}
