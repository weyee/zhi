package com.free4inno.knowledgems.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import java.sql.Timestamp;


/**
 * Author HaoYi.
 * Date 2020/9/11.
 */
@Entity
@Table(name = "resource")
@Data
public class Resource {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id; //主键

    @Column(name = "crop_id")
    private Integer cropId; //CropId

    @Column(name = "user_id")
    private Integer userId; //创作者id

    @Column(name = "create_time")
    private Timestamp createTime; //资源创建时间

    @Column(name = "edit_time")
    private Timestamp editTime; //资源修改时间

    @Column(name = "title", columnDefinition = "TEXT")
    private String title; //标题

    @Column(name = "text", columnDefinition = "LONGTEXT")
    private String text; //正文

    @Column(name = "attachment", columnDefinition = "TEXT")
    private String attachment; //附件字段，用来保存附件的url地址（一个或者多个），以json格式保存

    @Column(name = "superior")
    private Integer superior; //资源等级，用于区分资源是原贴还是评论（if =0，则为原帖；if=某资源id，则为评论）

    @Column(name = "recognition")
    private Integer recognition; //点赞数

    @Column(name = "opposition")
    private Integer opposition; //反对数

    @Column(name = "pageview")
    private Integer pageview; //浏览数

    @Column(name = "collection")
    private Integer collection; //收藏数

    @Column(name = "group_id", columnDefinition = "TEXT")
    private String groupId; //资源所属用户组id

    @Column(name = "label_id", columnDefinition = "TEXT")
    private String labelId; //资源的标签id

    @Column(name = "permissionId")
    private Integer permissionId; //资源是否为公开资源（0为非公开，1为公开）

    @Column(name = "contents", columnDefinition = "TEXT")
    private String contents; //资源书籍目录

    @Transient
    private String groupName; //用户组名称

    @Transient
    private String labelName; //资源标签名称

    @Transient
    private String userName; //用户名

    @Transient
    private Integer specType; //推荐

    @Transient
    private Integer specOrder; //推荐排序

    @Transient
    private Integer recommendType; //推荐

    @Transient
    private Integer recommendOrder; //推荐排序

    @Transient
    private Integer noticeType; //公告

    @Transient
    private Integer noticeOrder; //公告排序

    @Transient
    private Integer searchedByAttachment; //是否由附件字段命中

    @Transient
    private Integer searchedByText; //是否由正文字段命中

    @Transient
    private Integer searchedByComment; //是否由评论字段命中

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCropId() {
        return cropId;
    }

    public void setCropId(Integer cropId) {
        this.cropId = cropId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getEditTime() {
        return editTime;
    }

    public void setEditTime(Timestamp editTime) {
        this.editTime = editTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public Integer getSuperior() {
        return superior;
    }

    public void setSuperior(Integer superior) {
        this.superior = superior;
    }

    public Integer getRecognition() {
        return recognition;
    }

    public void setRecognition(Integer recognition) {
        this.recognition = recognition;
    }

    public Integer getOpposition() {
        return opposition;
    }

    public void setOpposition(Integer opposition) {
        this.opposition = opposition;
    }

    public Integer getPageview() {
        return pageview;
    }

    public void setPageview(Integer pageview) {
        this.pageview = pageview;
    }

    public Integer getCollection() {
        return collection;
    }

    public void setCollection(Integer collection) {
        this.collection = collection;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getLabelId() {
        return labelId;
    }

    public void setLabelId(String labelId) {
        this.labelId = labelId;
    }

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getSpecType() {
        return specType;
    }

    public void setSpecType(Integer specType) {
        this.specType = specType;
    }

    public Integer getSpecOrder() {
        return specOrder;
    }

    public void setSpecOrder(Integer specOrder) {
        this.specOrder = specOrder;
    }

    public Integer getRecommendType() {
        return recommendType;
    }

    public void setRecommendType(Integer recommendType) {
        this.recommendType = recommendType;
    }

    public Integer getRecommendOrder() {
        return recommendOrder;
    }

    public void setRecommendOrder(Integer recommendOrder) {
        this.recommendOrder = recommendOrder;
    }

    public Integer getNoticeType() {
        return noticeType;
    }

    public void setNoticeType(Integer noticeType) {
        this.noticeType = noticeType;
    }

    public Integer getNoticeOrder() {
        return noticeOrder;
    }

    public void setNoticeOrder(Integer noticeOrder) {
        this.noticeOrder = noticeOrder;
    }

    public Resource() {

    }

    public Resource(int id) {
        this.id = id;
    }

}
