<%--
  Created by IntelliJ IDEA.
  User: Creed
  Date: 2021/1/15
  Time: 23:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:import url="../template/_header.jsp"/>
    <title>新建群组-知了[团队知识管理应用]</title>
</head>
<body>

<div class="re-main">
    <c:import url="../template/_navbar.jsp"/>
    <div class="re-box-6 re-grey mt-85">
        <div class="container">
            <div class="row vertical-gap md-gap">
                <div class="col-lg-12">
                    <div class="re-box re-box-decorated">
                        <div class="resource-post">
                            <div class="resource-post-box pt-30">
                                <ul class="re-breadcrumbs pb-25" id="breadCrumb">
                                    <a href="../manage" >管理</a><a> | </a><a href="#" onclick="javascript :history.back(-1);">群组管理</a><a> | 新建群组</a>
                                </ul>
                                <h5 class="panel-heading"><strong>新建群组</strong></h5>
                                <div class="panel-body" style="margin-bottom: 40px">
                                    <div>
                                        <div class='form-group'>
                                            <label class="control-label front-label">群组名称</label>
                                            <div>
                                                <input type="text" class="form-control front-no-box-shadow" id="newName">
                                            </div>
                                        </div>

                                        <div class='form-group'>
                                            <label>群主</label>
                                            <span id="search-criteria"><a
                                                    style="text-decoration:none" href="javascript:void(0);"
                                                    onclick="showLeaderDialog()">设置</a></span>
                                            <div id="selectedName">未选择</div>
                                        </div>
                                        <a href="javascript:void(0)" class="btn btn-primary" id="saveButton" style="width: 80px; float: right; margin-bottom: 20px" onclick="addGroup();">新建</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="setLeader" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel" data>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">选择成员</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>成员姓名</label>
                    <select class="selectpicker form-control" id="leaderName" data-live-search="true"></select>
                </div>
            </div>
            <div class="modal-footer">
                <%--                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>关闭</button>--%>
                <button type="button" id="btn_submit" class="btn btn-primary" data-dismiss="modal"
                        onclick="returnName()"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span><a>确定</a></button>
            </div>
        </div>
    </div>
</div>
</body>
<c:import url="../template/_footer.jsp"/>
<c:import url="../template/_include_js.jsp"/>

<script>
    /*
* 最初加载页面的时候，进行的搜索
* */
    $(document).ready(function () {
        //loading
        $.ajax({
            type: 'post',
            url:"/getAllNames",
            dataType: "json",
            success: function (data) {
                // 获取数据成功时进行循环拼接下拉框;
                var tempIdStr = '<option  value="" selected：disable style="display: none">点击选择用户</option>';
                $("#leaderName").append(tempIdStr);
                for (var i = 0; i < data.length; i++) {
                    var add_name = '<option value="' + data[i].split(" ")[0] + '">'+ data[i] + '</option>';
                    $('#leaderName').append(add_name);
                }
                //这一步很重要   更新
                $('#leaderName').selectpicker('refresh');
            }
        })
    })

    function showLeaderDialog(){
        $('#setLeader').modal();
    }

    function returnName(){
        var leaderName = $("#leaderName").val();
        $("#selectedName").html(leaderName);

    }

    function addGroup(){
        var btn = $("#saveButton");
        btn.attr("disabled",true);
        var name = $("#newName").val();
        var leaderName = $("#leaderName").val();
        if(name=="" || leaderName==""){
            $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '项目名称及群主不可为空'});
            return false;
        }
        btn.attr("disabled",true);

        $.ajax({
            url:"/addGroup",
            type:"post",
            async: false,
            dataType:"text",
            data: {name: name,
                    leaderName: leaderName
                },
            success:function(data) {
                if(data=="ok") {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '创建群组成功'});
                    window.location.href = "<%=request.getContextPath()%>/groupmanage";
                }else{
                    $.fillTipBox({type: 'error', icon: 'glyphicon-alert', content: '创建群组失败'});
                }
            },
        });
    }
</script>
</html>
